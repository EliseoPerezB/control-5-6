#include <stdio.h>
#include <stdlib.h>
#include "estudiantes.h"
#include <math.h> //Libreria agregada para las funciones matematicas (pow y sqrt)


float desvStd(estudiante curso[]){  //Funcion para calcular la desviacion estandar
	float sumaProm, promedioCurso, difProm, elevProm, elevSuma, divProm, DesviacionStd; 
	for (int i = 0; i < 24; i++){
		sumaProm = curso[i].prom + sumaProm; //contador para saber el numero de notas, al momento de dividir
	}
	promedioCurso = sumaProm / 24; //promedio general del curso

	for (int i = 0; i < 24; i++){
		difProm = (curso[i].prom - promedioCurso); //diferencia entre el promedio del alumno y del promedio general
		elevProm = pow(difProm, 2);  //procedimiento para sacar la varianza
		elevSuma = (elevProm + elevSuma);
	}
	divProm = (elevSuma / 23);
	DesviacionStd = sqrt(divProm); //a todo el resultado anterior (divProm) se le saca la raiz cuadrada 
	return DesviacionStd;  
}

float menor(estudiante curso[]){ //Funcion para sacar el promedio mas bajo
	int menorProm; 
	for (int i = 0; i < 24; i++){ 
		if(curso[i].prom < menorProm){// compara para quedarse con el numero menor
			menorProm = curso[i].prom;
		}  
	}
	return menorProm; //retorna el promedio menor
}

float mayor(estudiante curso[]){ //Funcion para sacar el promedio mas alto
	int mayorProm = 0;
	for (int i = 0; i < 24; i++){
		if(curso[i].prom > mayorProm){ //compara para quedarse con el numero mayor
			mayorProm = curso[i].prom;
		}
	}
	return mayorProm; //retorna el promedio mayor 
}

void registroCurso(estudiante curso[]){ //Aca en esta funcion se piden las notas de los estudiantes
	for (int i = 0; i < 24; i++){ //ciclo for para los 24 estudiantes
		printf("\nIngrese la nota de los 3 proyectos : "); 
		scanf("%f %f %f", &curso[i].asig_1.proy1, &curso[i].asig_1.proy2, &curso[i].asig_1.proy3);
		printf("\nIngrese la nota del control 1: ");
		scanf("%f", &curso[i].asig_1.cont1);
		printf("\nIngrese la nota del control 2: ");
		scanf("%f", &curso[i].asig_1.cont2);
		printf("\nIngrese la nota del control 3: ");
		scanf("%f", &curso[i].asig_1.cont3);
		printf("\nIngrese la nota del control 4: ");
		scanf("%f", &curso[i].asig_1.cont4);
		printf("\nIngrese la nota del control 5: ");
		scanf("%f", &curso[i].asig_1.cont5);
		printf("\nIngrese la nota del control 6: ");
		scanf("%f", &curso[i].asig_1.cont6);
		curso[i].prom = (curso[i].asig_1.proy1*0.2) + (curso[i].asig_1.proy2*0.2) + (curso[i].asig_1.proy3*0.3) + (curso[i].asig_1.cont1*0.05) + (curso[i].asig_1.cont2*0.05) + (curso[i].asig_1.cont3*0.05) + (curso[i].asig_1.cont4*0.05) + (curso[i].asig_1.cont5*0.05) + (curso[i].asig_1.cont6*0.05);//Ponderaciones de las evaluaciones
		
	}
	printf("\n***** Utiliza la opcion 3 para saber los promedios, 4 para almacenar las notas y 5 paraver los reprobados y aprobados *****\n"); 

}

void clasificarEstudiantes(char path[], estudiante curso[]){ //Esta funcion crea archivos con alumnos Reprobados y Aprobados
	FILE *ListaReprobados;
	FILE *ListaAprobados;
	ListaReprobados = fopen("AlumnosReprobados.txt", "w"); 
	ListaAprobados = fopen("AlumnosAprobados.txt", "w");
	if(((ListaReprobados = fopen(path,"w")) == NULL) && ((ListaAprobados = fopen(path, "w")) == NULL)){//Condicion para el error, si es que existe alguno
		printf("\nerror al crear el archivo");
		exit(0); 
	}else{
		for (int i = 0; i<24; i++){
			if (curso[i].prom < 4.0){ //Condicion para los alumnos reprobados, promedio menor que 4, sino se cumple estan aprobados
				fprintf(ListaReprobados, "%s %s %s %f\n", curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM, curso[i].prom); //Se imprimen los nombres y apellidos de los alumnos reprobados mas su promedio
			}else{
				fprintf(ListaAprobados, "%s %s %s %f\n", curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM, curso[i].prom); //Se imprimen los nombres y apellidos de los alumnos aprobados mas su promedio
			}
		}
		fclose(ListaReprobados); 
		fclose(ListaAprobados);
	}
}


void metricasEstudiantes(estudiante curso[]){ //Muestra la Desviacion estandar, promedio menor y mayor
	float DesviacionStd, menorProm, mayorProm;
	DesviacionStd = desvStd (curso);//aca se llama a las funciones desvStd, menor, mayor
	menorProm = menor(curso);
	mayorProm = mayor(curso);
	printf("\nLa desviacion estandar del curso es: %f\n ", DesviacionStd);
	printf("\nEl promedio mas bajo es: %f\n ", menorProm);
	printf("\nEl promedio mas alto es: %f\n ", mayorProm);
}



void menu(estudiante curso[]){ //En esta funcion se le pide que escoja una opcion para realizar procesos necesarios
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso); //carga en un lote una lista en un arreglo de registro, junto con el promedio
					break;
            case 5: clasificarEstudiantes("AlumnosReprobados", curso); //Clasifica a los estudiantes en dos arreglos de registros distintos, dependiendo de su estado (reprobado o aprobado)******QUIERO RECALCAR, QUE ACA PUSE "ALUMNOSREPROBADOS", PUES ME IMPRIMIA EN EL ARCHIVO DESTINO QUE ESTABA EN ESTE LUGAR

            		break;
         }

    } while ( opcion != 6 ); //opcion para salir del programa
}

int main(){
	estudiante curso[30]; //declaracion del tamaño del arreglo
	menu(curso); //declaracion de la variable curso en la funcion menu
	return EXIT_SUCCESS; 
}